import logo from './logo.svg';
import React, { useState } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Login from '../Login';
import './App.css';

function App() {
    
    const [token, setToken] = useState();

    if(!token) {
      return <Login setToken={setToken} />
    }
  return (
    <div className="App">
        <BrowserRouter>
      <header className="App-header">  
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>         
      </header>
       </BrowserRouter>
    </div>
  );
}

export default App;
