# README #
# README #


El proyecto se ha realizado con Spring FrameWork y se ha utilizado una base de datos SQL Server.
La base de datos se llama Usuarios pero se podrá instalar la tabla sobre otra existente si ya se tiene configurado el DataSource
Se han creado tres usuarios a modo de ejemplo, la password es el mismo usuario, que son:

admin-$2a$10$3nIsU8KcH88GXwlpvUlvCe6mkNLMmCK8w3va/74DK7HvL2j0/sO4e. 
user-$2a$10$nrYIhbzXWN4FUNJl9kQFpen4rS8dxSCoPuu/X63.hkpfGE2n1dsqS
user2 -$2a$10$W7B5fln70jvmhVT/ULF8Luxo55Sto1HL0YorxjUlFCUAwjimA5wR.
Adjunto password encriptada para introducir en la tabla por si hay algun problema para importar los datos que se adjuntan el fichero timelogin.bacpac, dentro del proyecto.

La query para la creacion de la tabla es:
DROP TABLE [dbo].[Usuario]
GO

/****** Object:  Table [dbo].[Usuario]    Script Date: 09/09/2021 17:51:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Usuario](
	[id] [int] NOT NULL,
	[username] [nchar](10) NOT NULL,
	[password] [nvarchar](max) NULL,
	[acceso] [datetime] NOT NULL,
 CONSTRAINT [PK_Login] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

Y la insercion de datos, pues la password están encriptadas pero no se ha realizado la funcion de registro, es:

